import {createStore, combineReducers, compose , appylyMiddleware, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import placesReducers from './reducers/places'

const rootReducer = combineReducers({
    places: placesReducers
})

const composeEnhancers = compose;

if (__DEV__){
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

const configureStore = () => {
    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
}

export default configureStore;